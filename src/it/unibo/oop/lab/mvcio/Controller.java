package it.unibo.oop.lab.mvcio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    private final String HOME = System.getProperty("user.home");
    private final String SEPARATOR = System.getProperty("file.separator");
    private final String DEFAULT_FILE = "output.exe";
    private String path = HOME + SEPARATOR + DEFAULT_FILE;
    private File file = new File(HOME + SEPARATOR + DEFAULT_FILE);
    /**
     * Set the new file
     * @param newFile is the new file
     */
        public void setFile(final File newFile) {
                this.file = newFile;
                this.path = newFile.getPath();
	}
	/**
	 * Get the current file
	 * @return the file
	 */
	public File getFile() {
		return this.file;
	}
	/**
	 * Get the path of the file
	 * @return the path
	 */
	public String getPath() {
		return this.file.getPath();
	}
	/**
	 * Add a string in a file
	 * @param s is the string that is added
	 * @throws IOException throws IOException
	 */
	public void addToFile(final String s) throws IOException {
		try (
		    BufferedWriter w = new BufferedWriter(new FileWriter(this.getPath()));
        ) {
			w.write(s);
		  }
	}
}
