package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

/**
 * A controller that prints strings and has memory of the strings it printed.
 */
public interface Controller {

    /*
     * This interface must model a simple controller responsible of I/O access.
     * It considers only the standard output, and it is able to print on it.
     * 
     * Write the interface and implement it in a class class in such a way that
     * it includes:
     * 
     * 1) A method for setting the next string to print. Null values are not
     * acceptable, and an exception should be produced
     * 
     * 2) A method for getting the next string to print
     * 
     * 3) A method for getting the history of the printed strings (in form of a
     * List of Strings)
     * 
     * 4) A method that prints the current string. If the current string is
     * unset, an IllegalStateException should be thrown
     * 
     */
    /**
     * Class that represents the controller.
     */
     class Ctrl {
        private final List<String> strings = new ArrayList<>();
        private String printable;

        /**
         * Set the next string to be printed.
         * @throws Exception
         *                  exception
         */
        public void setString(final String s) {
            try {
                printable = s;
            } catch (NullPointerException e) {
                System.out.println("Empty strings are not accepted!");
                throw e;
            }
        }
        /**
         * 
         * @return the next string to be printed
         */
        public String getString() {
            return printable;
        }
        /**
         * 
         * @return all the strings that were printed
         */
        public List<String> getAllStrings() {
            return strings;
        }
        /**
         * Prints the current string.
         */
        public void printString() {
            try {
                System.out.println(printable);
                strings.add(printable);
            } catch (IllegalStateException e) {
                throw e;
            }
        }
    }
}
